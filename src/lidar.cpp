#include "lidar.h"

//point cloud callback
void lidar::pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr& msg) {

    //planarRegions - computes:
        //planeParameters
        //clusters
        //iPlanes
        //nPlanes
        //cloud
        //timestamps
    
    std::vector <pcl::PointIndices> clusters;
    clusters = lidar::planarRegions(msg);

    std::cerr << "\n=========================================================";
    std::cerr << "\n Points: " << points.cols();
    std::cerr << "\n Dirs: " << dirs.cols();
    std::cerr << "\n Times: " << timestamps.size();
    std::cerr << "\n nRegions: " << nRegions;
    std::cerr << "\n nPlanes: " << nPlanes;
    std::cerr << "\n=========================================================";


    // 0. ICP Prior
    sensor_msgs::PointCloud2 pointcloud = *msg;
    pcl::PointCloud<pcl::PointXYZ>::Ptr prevCloudPtr (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr currentCloudPtr (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (pointcloud,*currentCloudPtr);
    Vector idVec;
    Pose3 poseRel;
    if (scanCount == 0) {
        idVec = gtsam::zero(6);
        poseRel = Pose3::Expmap(idVec);
    }
    else {
        pcl::fromROSMsg (prevPointcloud,*prevCloudPtr);
        poseRel = lidar::scanOdom(currentCloudPtr,prevCloudPtr);
        // poseRel = lidar::scanOdom(prevCloudPtr,currentCloudPtr);
    }
    prevPointcloud = *msg;

    //=========================================================================================
    // 1. Initialization
    Pose3 poseInit;
    if (scanCount == 0) {
        Vector initVec = gtsam::zero(6);
        poseInit  = Pose3::Expmap(initVec);
    }
    else {
        poseInit  = prevPose;
    }

    Pose3 poseFinal = poseInit.transform_to(poseRel);
    // Pose3 poseFinal = poseRel.transform_to(poseInit);
    prevPose = poseFinal;

    Matrix4 T = poseFinal.matrix();
    static tf::TransformBroadcaster br;
    tf::Transform transform;
    transform.setOrigin( tf::Vector3(T(0,3), T(1,3), T(2,3)) );
    tf::Matrix3x3 m;
    m.setValue(T(0,0),T(0,1),T(0,2),
               T(1,0),T(1,1),T(1,2),
               T(2,0),T(2,1),T(2,2));
    transform.setBasis(m);
    br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "velodyne", "lidar"));

    // No ICP prior
    // Vector initVec = gtsam::zero(6);
    // poseInit  = Pose3::Expmap(initVec);
    // Vector relVec = gtsam::zero(6);
    // relVec(0) = 1.1;
    // relVec(1) = 0;
    // relVec(2) = 0;
    // relVec(3) = 0.3;
    // relVec(4) = 0.2;
    // relVec(5) = 0.3;
    // Pose3 poseRel2   = Pose3::Expmap(relVec);
    // Pose3 poseFinal = poseRel2.transform_to(poseInit);
    // prevPose = poseFinal;
    

    // 2. unwarp
    Matrix correctedPoints;
    Matrix posesEst;
    Pose3 lastPose;

    tie(correctedPoints,posesEst,lastPose) = unwarpLidar(points,poseInit,poseFinal,timestamps,clusters,iPlanes);

    // prevPose = lastPose;

    // //broadcast tf
    // Matrix4 T = poseFinal.matrix();
    // static tf::TransformBroadcaster br;
    // tf::Transform transform;
    // transform.setOrigin( tf::Vector3(T(0,3), T(1,3), T(2,3)) );
    // tf::Matrix3x3 m;
    // m.setValue(T(0,0),T(0,1),T(0,2),
    //            T(1,0),T(1,1),T(1,2),
    //            T(2,0),T(2,1),T(2,2));
    // transform.setBasis(m);
    // br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "velodyne", "lidar"));


    // 3. create and publish corrected point cloud
    pcl::PointCloud<pcl::PointXYZ> correctedCloud;

    for (int i = 0; i < correctedPoints.cols(); i++) {
        double x,y,z;
        x = correctedPoints(0,i);
        y = correctedPoints(1,i);
        z = correctedPoints(2,i);

        //store
        correctedCloud.push_back (pcl::PointXYZ (x, y, z));

    }

    //4. publish
    sensor_msgs::PointCloud2 correctedCloudMsg;
    pcl::toROSMsg(correctedCloud, correctedCloudMsg);
    correctedCloudMsg.header.frame_id = "/velodyne";
    lidar::corrected_pointcloud_pub.publish(correctedCloudMsg);


    //5. visualise
    // //original
    // // sensor_msgs::PointCloud2 originalCloud = *msg;
    // // pcl::PointCloud<pcl::PointXYZ>::Ptr ptrOriginalCloud (new pcl::PointCloud<pcl::PointXYZ>);
    // // pcl::fromROSMsg (originalCloud,*ptrOriginalCloud );
    // //corrected
    // pcl::PointCloud<pcl::PointXYZ>::Ptr ptrCloud(&correctedCloud);
    // //view
    // pcl::visualization::CloudViewer viewer ("spherical cloud viewer");
    // // viewer.showCloud(ptrOriginalCloud, "ptrOriginalCloud");
    // viewer.showCloud(ptrCloud, "ptrCloud");
    // while (!viewer.wasStopped ())
    // {
    // }


}


Pose3 lidar::scanOdom(pcl::PointCloud<pcl::PointXYZ>::Ptr prevCloudPtr, pcl::PointCloud<pcl::PointXYZ>::Ptr currentCloudPtr) {

    // Vector initVec = gtsam::zero(6);
    // Pose3 poseInit  = Pose3::Expmap(initVec);

    std::cerr << "\n=========================================================";
    std::cerr << "\n ICP";
    std::cerr << "\n=========================================================";

    //view
    // pcl::visualization::CloudViewer viewer ("spherical cloud viewer");
    // viewer.showCloud(prevCloudPtr, "prevCloudPtr");
    // // viewer.showCloud(currentCloudPtr, "currentCloudPtr");
    // while (!viewer.wasStopped ())
    // {
    // }

    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;
    icp.setMaximumIterations(5);
    // icp.setTransformationEpsilon (1e-9);
    // icp.setMaxCorrespondenceDistance (1);
    // icp.setEuclideanFitnessEpsilon (0.5);
    // icp.setRANSACOutlierRejectionThreshold (0.01);
    icp.setInputSource(prevCloudPtr);
    icp.setInputTarget(currentCloudPtr);
    pcl::PointCloud<pcl::PointXYZ> Final;
    icp.align(Final);
    if (icp.hasConverged()) {
        std::cout << "\nICP converged." << std::endl << "The score is " << icp.getFitnessScore() << std::endl;
        std::cout << "Transformation matrix:" << std::endl;
        std::cout << icp.getFinalTransformation() << std::endl;
    }
    else std::cout << "ICP did not converge." << std::endl;

    Eigen::Matrix4f T = icp.getFinalTransformation();
    Matrix T2 = gtsam::zeros(4,4);
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            T2(i,j) = T(i,j);
        }
    }
    Pose3 poseInit = Pose3(T2);

    return poseInit;

}



std::vector <pcl::PointIndices> lidar::planarRegions(const sensor_msgs::PointCloud2ConstPtr& msg) {
    //region segmentation
    std::vector <pcl::PointIndices> clusters;
    clusters = lidar::regionSegmentation(msg);

    //get cloud
    sensor_msgs::PointCloud2 pointcloud = *msg;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (pointcloud,*cloud);

    //init
    nPlanes = 0;
    iPlanes = gtsam::zero(nRegions);
    // iPlanes = gtsam::zeros(1,nRegions);
    planeParameters = gtsam::zeros(4,nRegions);
    Matrix planeErrors = gtsam::zeros(1,nRegions);

    //loop over regions
    for (int i = 0; i < nRegions; i++) {
        //fit plane to region
        Matrix iParameters;
        Vector iErrors;
        tie(iParameters,iErrors) = lidar::fitPlanesFromCloud(cloud,clusters,i); 
        gtsam::insertColumn(planeParameters,iParameters,i);

        //compute average distance to plane
        double errorMean = gtsam::sum(gtsam::abs(iErrors))/clusters[i].indices.size();
        planeErrors(0,i) = errorMean;

        if (errorMean < 0.01) {
            // iPlanes(0,nPlanes) = i;
            iPlanes(nPlanes) = i;
            nPlanes++;
        }

    }

    //adjust size
    iPlanes = gtsam::sub(iPlanes,0,nPlanes);

    return clusters;

}

    std::vector <pcl::PointIndices> lidar::regionSegmentation(const sensor_msgs::PointCloud2ConstPtr& msg) {
    //get pointcloud    
    sensor_msgs::PointCloud2 pointcloud = *msg;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromROSMsg (pointcloud,*cloud );

    //region growing segmentation code
    pcl::search::Search<pcl::PointXYZ>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZ> > (new pcl::search::KdTree<pcl::PointXYZ>);
    pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimator;
    normal_estimator.setSearchMethod (tree);
    normal_estimator.setInputCloud (cloud);
    normal_estimator.setKSearch (50);
    normal_estimator.compute (*normals);

    pcl::IndicesPtr indices (new std::vector <int>);
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud (cloud);
    pass.setFilterFieldName ("z");
    pass.setFilterLimits (0.0, 1.0);
    pass.filter (*indices);

    pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
    reg.setMinClusterSize (50);
    reg.setMaxClusterSize (1000000);
    reg.setSearchMethod (tree);
    reg.setNumberOfNeighbours (30);
    reg.setInputCloud (cloud);
    //reg.setIndices (indices);
    reg.setInputNormals (normals);
    reg.setSmoothnessThreshold (3.0 / 180.0 * M_PI);
    reg.setCurvatureThreshold (1.0);

    std::vector <pcl::PointIndices> clusters;
    reg.extract (clusters);

    //nRegions
    nRegions = clusters.size(); 

    //visualise
    // pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = reg.getColoredCloud ();
    // pcl::visualization::CloudViewer viewer ("Cluster viewer");
    // viewer.showCloud(colored_cloud);
    // while (!viewer.wasStopped ())
    // {
    // }

    return clusters;

}

void lidar::scanCallback(const spherical_scan_msgs::Scan& msg) {
    scanCount++;
    std::cerr << "\n=========================================================";
    std::cerr << "\n SCAN: " << scanCount;
    std::cerr << "\n=========================================================";

    // if (scanCount==1) {

    spherical_scan_msgs::Scan scan = msg;
    int nPoints = scan.points.size();
    pcl::PointCloud<pcl::PointXYZ> cloud;
    // pcl::PointCloud<pcl::PointXYZ> dirs;
    // ros::Time timestamps[nPoints];
    timestamps = gtsam::zero(nPoints);

    points = gtsam::zeros(3,nPoints);
    dirs = gtsam::zeros(3,nPoints);

    for (int i = 0; i < nPoints; i++) {
        // timestamps[i] = scan.points[i].time_stamp;
        timestamps(i) = scan.points[i].time_stamp.toSec();

        // extract coordinates
        double radius,azimuth,polar,x,y,z;
        radius  = scan.points[i].position.radius;
        azimuth = scan.points[i].position.azimuth;
        polar = scan.points[i].position.polar;

        // convert to x,y,z
        x = radius * cos(azimuth) * sin(polar);
        y = radius * sin(azimuth) * sin(polar);
        z = radius * cos(polar);

        //store
        cloud.push_back (pcl::PointXYZ (x, y, z));
        // dirs.push_back (pcl::PointXYZ (x/radius, y/radius, z/radius));
        points(0,i) = x;
        points(1,i) = y;
        points(2,i) = z;
        dirs(0,i) = x/radius;
        dirs(1,i) = y/radius;
        dirs(2,i) = z/radius;

    }

    //FOR TESTING
    // pcl::PointCloud<pcl::PointXYZ>::Ptr simCloud (new pcl::PointCloud<pcl::PointXYZ>);
    // if ( pcl::io::loadPCDFile <pcl::PointXYZ> ("/home/monty/Data/simVelodyne16/gtCloud.pcd", *simCloud) == -1) {
    //     std::cout << "Cloud reading failed." << std::endl;
    // }

    //visualise
    // pcl::PointCloud<pcl::PointXYZ>::Ptr ptrCloud(&cloud);
    // pcl::visualization::CloudViewer viewer ("spherical cloud viewer");
    // // viewer.showCloud(ptrCloud);
    // viewer.showCloud(simCloud);
    // while (!viewer.wasStopped ())
    // {
    // }

    //TODO PUBLISH POINTCLOUD & TIMESTAMPS IN CUSTOM MESSAGE
    sensor_msgs::PointCloud2 cloud2;
    pcl::toROSMsg(cloud, cloud2);
    // pcl::toROSMsg(*simCloud, cloud2);
    // pcl::toROSMsg(dirs, cloud2);
    lidar::pointcloud_pub.publish(cloud2);
    
    // }//if scancount
}

std::tuple<Matrix,Vector> lidar::fitPlanesFromCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::vector <pcl::PointIndices> clusters, int iRegion) {
    //this function fits plane to points & computes signed distance from each point to plane

    int nPoints = clusters[iRegion].indices.size();
    //subtract centroid from points
    Matrix iPoints = gtsam::zeros(3,nPoints);
    for (int j = 0; j < clusters[iRegion].indices.size(); j++) {
        iPoints(0,j) = cloud->points[clusters[iRegion].indices[j]].x;
        iPoints(1,j) = cloud->points[clusters[iRegion].indices[j]].y;
        iPoints(2,j) = cloud->points[clusters[iRegion].indices[j]].z;
    }

    Matrix parameters;
    Vector errors;
    tie(parameters,errors) = lidar::fitPlane(iPoints);
    
    return std::make_tuple(parameters,errors);
}

std::tuple<Matrix,Vector> lidar::fitPlane(Matrix planePoints) {
    int nPoints = planePoints.cols();
    Vector xVals = gtsam::row(planePoints,0);
    Vector yVals = gtsam::row(planePoints,1);
    Vector zVals = gtsam::row(planePoints,2);
    double xMean = gtsam::sum(xVals)/nPoints;
    double yMean = gtsam::sum(yVals)/nPoints;
    double zMean = gtsam::sum(zVals)/nPoints;
    Matrix pointsMean = zeros(3,1);
    pointsMean(0,0) = xMean; pointsMean(1,0) = yMean; pointsMean(2,0) = zMean;
    Vector xShift = xVals - xMean*gtsam::ones(nPoints);
    Vector yShift = yVals - yMean*gtsam::ones(nPoints);
    Vector zShift = zVals - zMean*gtsam::ones(nPoints);
    Matrix pointsShiftT = zeros(nPoints,3);
    gtsam::insertColumn(pointsShiftT,xShift,0);
    gtsam::insertColumn(pointsShiftT,yShift,1);
    gtsam::insertColumn(pointsShiftT,zShift,2);
    Matrix pointsShift = gtsam::trans(pointsShiftT);
    //SVD
    Matrix R = pointsShift*pointsShiftT;
    Eigen::JacobiSVD<Matrix> svd(R, Eigen::ComputeThinU | Eigen::ComputeThinV);
    Matrix V = svd.matrixV();
    Matrix parameters = zeros(4,1);
    Matrix n = gtsam::column(V,2);
    Matrix d = gtsam::trans(n)*pointsMean;
    parameters(0,0) = n(0,0);
    parameters(1,0) = n(1,0);
    parameters(2,0) = n(2,0);
    parameters(3,0) = d(0,0);

    //compute errors
    Vector errors = gtsam::trans(planePoints)*n - d(0)*gtsam::ones(nPoints,1);

    return std::make_tuple(parameters,errors);

}

std::tuple<Vector,Matrix> lidar::initControlPoints(int nControlPoints, double tStart, double tEnd, Pose3 poseInit, Pose3 poseFinal) { 

    //init outputs
    Vector tControlPoints = gtsam::zero(nControlPoints);
    Matrix controlPoints = gtsam::zeros(6,nControlPoints);

    //start and end poses
    Vector poseVecInit = Pose3::Logmap(poseInit);
    Vector poseVecFinal = Pose3::Logmap(poseFinal);
    std::vector<double> tEndpoints(2),xEndpoints(2),yEndpoints(2),zEndpoints(2),wxEndpoints(2),wyEndpoints(2),wzEndpoints(2);
    tEndpoints[0] = tStart; tEndpoints[1] = tEnd;
    wxEndpoints[0] = poseVecInit(0); wxEndpoints[1] = poseVecFinal(0);
    wyEndpoints[0] = poseVecInit(1); wyEndpoints[1] = poseVecFinal(1);
    wzEndpoints[0] = poseVecInit(2); wzEndpoints[1] = poseVecFinal(2);
    xEndpoints[0] = poseVecInit(3); xEndpoints[1] = poseVecFinal(3);
    yEndpoints[0] = poseVecInit(4); yEndpoints[1] = poseVecFinal(4);
    zEndpoints[0] = poseVecInit(5); zEndpoints[1] = poseVecFinal(5);

    //spline
    tk::spline s0,s1,s2,s3,s4,s5;
    s0.set_points(tEndpoints,wxEndpoints);
    s1.set_points(tEndpoints,wyEndpoints);
    s2.set_points(tEndpoints,wzEndpoints);
    s3.set_points(tEndpoints,xEndpoints);
    s4.set_points(tEndpoints,yEndpoints);
    s5.set_points(tEndpoints,zEndpoints);
    
    //values
    for (int i = 0; i < nControlPoints; i++) {
        tControlPoints(i) = (double) i/((double) nControlPoints-1) * tEnd;
        controlPoints(0,i) = s0(tControlPoints(i));
        controlPoints(1,i) = s1(tControlPoints(i));
        controlPoints(2,i) = s2(tControlPoints(i));
        controlPoints(3,i) = s3(tControlPoints(i));
        controlPoints(4,i) = s4(tControlPoints(i));
        controlPoints(5,i) = s5(tControlPoints(i));
    }

    return std::make_tuple(tControlPoints,controlPoints);

}


std::tuple<Matrix,Matrix,Pose3> lidar::unwarpLidar(Matrix relativePoints,Pose3 poseInit,Pose3 poseFinal, Vector pointTimes, std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes) {
    //========================================================================================================
    // 1. initialize control points
    int nControlPoints = 4;
    int nPoints = points.cols();

    //start and end time
    double startTime = pointTimes(0);
    for (int i = 0; i < nPoints; i++) {
        pointTimes(i) = pointTimes(i) - startTime;
    }
    double tStart = pointTimes(0);
    double tEnd   = pointTimes(nPoints-1); 

    Vector tControlPoints;
    Matrix controlPoints;
    tie(tControlPoints,controlPoints) = lidar::initControlPoints(nControlPoints,tStart,tEnd,poseInit,poseFinal);

    //========================================================================================================
    // 2. solve for optimum control points
    // controlPoints = lidar::solveLM(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
    controlPoints = lidar::solveGN(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
    // controlPoints = lidar::solveDL(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);

    Pose3 finalPose = Pose3::Expmap(controlPoints.col(nControlPoints));

    //========================================================================================================
    // 3. compute poses and corrected points + publish corrected pointcloud
    Matrix correctedPoints;
    Matrix posesEst;
    tie(correctedPoints,posesEst) = lidar::correctPoints(controlPoints,poseInit,poseFinal,relativePoints,pointTimes);

    return std::make_tuple(correctedPoints,posesEst,finalPose);

}

Matrix lidar::solveLM(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes,std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes) {

    //initialise
    int iteration = 0;
    double lambda = 1e-8;
    double lambdaUp = 8.0;
    double lambdaDown = 12.0;
    double maxLambda = 1e50;
    double minLambda = 1e-50;
    double thresh = 1e-3;
    int maxIt = 10;
    double maxNormDX = 1e4;

    //vectorise control points - skip first col, don't optimise initial pose
    Vector x = gtsam::zero(controlPoints.rows()*(controlPoints.cols()-1));
    for (int i = 0; i < x.size(); i++) { 
        int row = i % 6;
        int col = i / 6 + 1;
        x(i) = controlPoints(row,col);
    }
    // Vector x = gtsam::zero(controlPoints.rows()*(controlPoints.cols()));
    // for (int i = 0; i < x.size(); i++) { 
    //     int row = i % 6;
    //     int col = i / 6;
    //     x(i) = controlPoints(row,col);
    // }


    Matrix A;
    Vector b;
    A = lidar::computeJacobian(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
    b = lidar::computeResiduals(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
    Matrix cov = gtsam::eye(A.rows(),A.rows());
    // A = cov*A;
    // b = cov*b;
    Matrix H = A.transpose()*A;
    Vector c = A.transpose()*b;
    double errorCurrent = b.norm();

    Matrix d = zeros(H.rows(),H.rows());
    Vector dX = gtsam::zero(controlPoints.rows()*(controlPoints.cols()-1));
    Vector xUpdate = gtsam::zero(controlPoints.rows()*(controlPoints.cols()-1));
    Matrix controlPointsUpdate = gtsam::zeros(controlPoints.rows(),controlPoints.cols());
    Matrix correctedPoints;
    Matrix posesEst;
    double errorTemp, rho;

    //LM
    while (true) {
        //construct linear system and solve
        for (int i = 0; i < H.rows(); i++) {
            d(i,i) = lambda*H(i,i);
        }
        dX = (H + d).fullPivLu().solve(c);
        //reduce update on first control point
        // for (int i = 0; i < 6; i++) {
        //     dX(i)*=0.0; 
        // }
        xUpdate = x - dX;
        controlPointsUpdate = controlPoints;
        for (int i = 0; i < x.size(); i++) {
            int row = i % 6;
            int col = i / 6 + 1;
            controlPointsUpdate(row,col) = xUpdate(i);
        }
        // for (int i = 0; i < x.size(); i++) {
        //     int row = i % 6;
        //     int col = i / 6;
        //     controlPointsUpdate(row,col) = xUpdate(i);
        // }
        errorTemp = (lidar::computeResiduals(controlPointsUpdate,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes)).norm();

        //update
        rho = errorCurrent - errorTemp;
        if (rho > 0) {
            //use update
            lambda/=lambdaDown;
            errorCurrent = errorTemp;
            x = xUpdate;
            controlPoints = controlPointsUpdate;
            A = lidar::computeJacobian(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
            b = lidar::computeResiduals(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
            // A = cov*A;
            // b = cov*b;
            H = A.transpose()*A;
            c = A.transpose()*b;
            iteration++;     

                    //debugging
            //---------------------------------------------------------------------------------------------------
            //generate current pointcloud - publish
            tie(correctedPoints,posesEst) = lidar::correctPoints(controlPoints,poseInit,poseFinal,relativePoints,pointTimes);
            pcl::PointCloud<pcl::PointXYZ> correctedCloud;
            for (int i = 0; i < correctedPoints.cols(); i++) {
                double px,py,pz;
                px = correctedPoints(0,i);
                py = correctedPoints(1,i);
                pz = correctedPoints(2,i);
                //store
                correctedCloud.push_back (pcl::PointXYZ (px, py, pz));
            }
            sensor_msgs::PointCloud2 correctedCloudMsg;
            pcl::toROSMsg(correctedCloud, correctedCloudMsg);
            correctedCloudMsg.header.frame_id = "/velodyne";
            lidar::corrected_pointcloud_pub.publish(correctedCloudMsg);
            //--------------------------------------------------------------------------------------------------


        } 
        else {
            //dont use update
            lambda*=lambdaUp;
        }

        //display
        std::cerr << "\nIt: " << iteration << ", |dX|: " << dX.norm() << ", |b|: " << errorCurrent << ", rho: " << rho << ", lambda: " << lambda;

        //termination criteria
        // if ((errorCurrent < 100) || (iteration > maxIt) || (dX.norm() > maxNormDX) || (lambda > maxLambda) || (lambda < minLambda)) {
        if ((dX.norm() < thresh) || (iteration > maxIt) || (dX.norm() > maxNormDX) || (lambda > maxLambda) || (lambda < minLambda)) {
            break;
        }

    }

    return controlPoints;

}


Matrix lidar::solveGN(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes,std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes) {

    //initialise
    int iteration = 0;
    double thresh = 1e-3;
    int maxIt = 10;
    double maxNormDX = 1e4;

    //vectorise control points - skip first col, don't optimise initial pose
    Vector x = gtsam::zero(controlPoints.rows()*(controlPoints.cols()-1));
    for (int i = 0; i < x.size(); i++) { 
        int row = i % 6;
        int col = i / 6 + 1;
        x(i) = controlPoints(row,col);
    }
    // Vector x = gtsam::zero(controlPoints.rows()*(controlPoints.cols()));
    // for (int i = 0; i < x.size(); i++) { 
    //     int row = i % 6;
    //     int col = i / 6;
    //     x(i) = controlPoints(row,col);
    // }


    Matrix A;
    Vector b;
    A = lidar::computeJacobian(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
    b = lidar::computeResiduals(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
    Matrix cov = gtsam::eye(A.rows(),A.rows());
    // A = cov*A;
    // b = cov*b;
    Matrix H = A.transpose()*A;
    Vector c = A.transpose()*b;
    double errorCurrent = b.norm();
    double errorPrev = errorCurrent;

    Vector dX = gtsam::zero(controlPoints.rows()*(controlPoints.cols()-1));
    Vector xUpdate = gtsam::zero(controlPoints.rows()*(controlPoints.cols()-1));
    Matrix controlPointsUpdate = gtsam::zeros(controlPoints.rows(),controlPoints.cols());
    Matrix correctedPoints;
    Matrix posesEst;

    //GN
    while (true) {
        //construct linear system and solve
        dX = (H).fullPivLu().solve(c);
        //reduce update on first control point
        // for (int i = 0; i < 6; i++) {
        //     dX(i)*=0.0; 
        // }

        xUpdate = x - dX;
        controlPointsUpdate = controlPoints;
        for (int i = 0; i < x.size(); i++) {
            int row = i % 6;
            int col = i / 6 + 1;
            controlPointsUpdate(row,col) = xUpdate(i);
        }
        // for (int i = 0; i < x.size(); i++) {
        //     int row = i % 6;
        //     int col = i / 6;
        //     controlPointsUpdate(row,col) = xUpdate(i);
        // }
        errorCurrent = (lidar::computeResiduals(controlPointsUpdate,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes)).norm();

        //use update
        if (errorCurrent <= errorPrev) {
            x = xUpdate;
            controlPoints = controlPointsUpdate;
        }
        else {
            break;
        }
        // x = xUpdate;
        // controlPoints = controlPointsUpdate;

        //debugging
        //---------------------------------------------------------------------------------------------------
        //generate current pointcloud - publish
        tie(correctedPoints,posesEst) = lidar::correctPoints(controlPoints,poseInit,poseFinal,relativePoints,pointTimes);
        pcl::PointCloud<pcl::PointXYZ> correctedCloud;
        for (int i = 0; i < correctedPoints.cols(); i++) {
            double px,py,pz;
            px = correctedPoints(0,i);
            py = correctedPoints(1,i);
            pz = correctedPoints(2,i);
            //store
            correctedCloud.push_back (pcl::PointXYZ (px, py, pz));
        }
        sensor_msgs::PointCloud2 correctedCloudMsg;
        pcl::toROSMsg(correctedCloud, correctedCloudMsg);
        correctedCloudMsg.header.frame_id = "/velodyne";
        lidar::corrected_pointcloud_pub.publish(correctedCloudMsg);
        //--------------------------------------------------------------------------------------------------

        A = lidar::computeJacobian(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
        b = lidar::computeResiduals(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
        // A = cov*A;
        // b = cov*b;
        H = A.transpose()*A;
        c = A.transpose()*b;
        iteration++;       

        //display
        std::cerr << "\nIt: " << iteration << ", |dX|: " << dX.norm() << ", |b|: " << errorCurrent;

        //termination criteria
        // if ((errorCurrent < 100) || (iteration > maxIt) || (dX.norm() > maxNormDX) || (lambda > maxLambda) || (lambda < minLambda)) {
        if ((dX.norm() < thresh) || (iteration > maxIt) || (dX.norm() > maxNormDX)) {
            break;
        }

    }

    return controlPoints;


}


Matrix lidar::solveDL(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes,std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes) {

    //initialise
    int iteration = 0;
    double lambda = 1e-8;
    double lambdaUp = 8.0;
    double lambdaDown = 12.0;
    double maxLambda = 1e50;
    double minLambda = 1e-50;
    double thresh = 1e-3;
    int maxIt = 25;
    double maxNormDX = 1e4;

    //vectorise control points - skip first col, don't optimise initial pose
    Vector x = gtsam::zero(controlPoints.rows()*(controlPoints.cols()-1));
    for (int i = 0; i < x.size(); i++) { 
        int row = i % 6;
        int col = i / 6 + 1;
        x(i) = controlPoints(row,col);
    }
    // Vector x = gtsam::zero(controlPoints.rows()*(controlPoints.cols()));
    // for (int i = 0; i < x.size(); i++) { 
    //     int row = i % 6;
    //     int col = i / 6;
    //     x(i) = controlPoints(row,col);
    // }


    Matrix A;
    Vector b;
    A = lidar::computeJacobian(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
    b = lidar::computeResiduals(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
    Matrix cov = gtsam::eye(A.rows(),A.rows());
    // A = cov*A;
    // b = cov*b;
    Matrix H = A.transpose()*A;
    Vector c = A.transpose()*b;
    double errorCurrent = b.norm();
    std::cerr << "\n=================================================================================";
    std::cerr << "\nbNORM: " << errorCurrent;
    // std::cerr << "\nA size: " << A.rows() << ", " << A.cols();
    // std::cerr << "\nb size: " << b.rows() << ", " << b.cols();
    // std::cerr << "\nA sum: " << A.sum();
    // std::cerr << "\nb sum: " << b.sum();
    // std::cerr << "\nH: " << H;
    // std::cerr << "\nc: " << c;
    std::cerr << "\n=================================================================================";


    //LM
    while (true) {
        //construct linear system and solve
        Vector dXGN = (H).fullPivLu().solve(c);
        //reduce update on first control point
        // for (int i = 0; i < 6; i++) {
        //     dX(i)*=0.0; 
        // }
        Vector dXSD = -2*c;
        Vector dX = dXGN;


        Vector xUpdate = x + dX;
        Matrix controlPointsUpdate = controlPoints;
        for (int i = 0; i < x.size(); i++) {
            int row = i % 6;
            int col = i / 6 + 1;
            controlPointsUpdate(row,col) = xUpdate(i);
        }
        // for (int i = 0; i < x.size(); i++) {
        //     int row = i % 6;
        //     int col = i / 6;
        //     controlPointsUpdate(row,col) = xUpdate(i);
        // }
        double errorTemp = (lidar::computeResiduals(controlPointsUpdate,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes)).norm();

        //update
        double rho = errorCurrent - errorTemp;
        if (rho > 0) {
            //use update
            lambda/=lambdaDown;
            errorCurrent = errorTemp;
            x = xUpdate;
            controlPoints = controlPointsUpdate;
            A = lidar::computeJacobian(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
            b = lidar::computeResiduals(controlPoints,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
            // A = cov*A;
            // b = cov*b;
            Matrix H = A.transpose()*A;
            Vector c = A.transpose()*b;
            iteration++;       
        } 
        else {
            //dont use update
            lambda*=lambdaUp;
        }

        //display
        std::cerr << "\n==========================================================================================";
        std::cerr << "\nIt: " << iteration << ", |dX|: " << dX.norm() << ", |b|: " << errorCurrent << ", rho: " << rho << ", lambda: " << lambda;
        std::cerr << "\n==========================================================================================";

        //termination criteria
        // if ((errorCurrent < 100) || (iteration > maxIt) || (dX.norm() > maxNormDX) || (lambda > maxLambda) || (lambda < minLambda)) {
        if ((dX.norm() < thresh) || (iteration > maxIt) || (dX.norm() > maxNormDX) || (lambda > maxLambda) || (lambda < minLambda)) {
            std::cerr << "\n==========================================================================================";
            std::cerr << "\n DONE";
            std::cerr << "\n==========================================================================================";
            break;
        }

    }

    return controlPoints;


}



Matrix lidar::computeJacobian(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes, std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes){

    //init A
    int nPlanePoints = 0;
    for (int i = 0; i < nPlanes; i++) {
        nPlanePoints = nPlanePoints + planeIndexes[iPlanes(i)].indices.size();
    }
    int dimCP = controlPoints.rows()*(controlPoints.cols()-1);
    // int dimCP = controlPoints.rows()*controlPoints.cols();
    Matrix A = gtsam::zeros(nPlanePoints,dimCP);

    // double h = 1e-4;
    double h = 1e-10;

    //compute numerical Jacobian
    for (int i = 0; i < dimCP; i++) {
        Matrix controlPointsPerturbed1 = controlPoints;
        Matrix controlPointsPerturbed2 = controlPoints;
        int row = i % 6;
        int col = i / 6 + 1;
        // int col = i / 6;
        controlPointsPerturbed1(row,col) += h;
        controlPointsPerturbed2(row,col) -= h;
        Vector r1,r2;
        r1 = lidar::computeResiduals(controlPointsPerturbed1,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
        r2 = lidar::computeResiduals(controlPointsPerturbed2,poseInit,poseFinal,relativePoints,pointTimes,planeIndexes,iPlanes);
        A.col(i) = (r1 - r2)/(2.0*h);
    }

    return A;

}

Vector lidar::computeResiduals(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes, std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes){
    //use planeIndexes & iPlanes OR compute new each time???

    //init
    int nPoints = relativePoints.cols();
    int nPlanes = iPlanes.size();

    //correct points
    Matrix correctedPoints;
    Matrix posesEst;
    tie(correctedPoints,posesEst) = lidar::correctPoints(controlPoints,poseInit,poseFinal,relativePoints,pointTimes);

    //init b
    int nPlanePoints = 0;
    for (int i = 0; i < nPlanes; i++) {
        nPlanePoints = nPlanePoints + planeIndexes[iPlanes(i)].indices.size();
    }
    Vector b = gtsam::zero(nPlanePoints);

    //fit planes to correctedPoints
    int bIndex = 0;
    for (int i = 0; i < nPlanes; i++) {
        //extract points in this plane
        Matrix iPoints = gtsam::zeros(3,planeIndexes[iPlanes(i)].indices.size());
        for (int j = 0; j < planeIndexes[iPlanes(i)].indices.size(); j++) {
            iPoints.col(j) = correctedPoints.col(planeIndexes[iPlanes(i)].indices[j]);
        }
        //fit plane
        Matrix parameters;
        Vector errors;
        tie(parameters,errors) = lidar::fitPlane(iPoints);

        //store errors in b
        for (int j = 0; j < planeIndexes[iPlanes(i)].indices.size(); j++) {
            b(bIndex) = errors(j);
            bIndex++;
        }

    }
   
    return b;

}

std::tuple<Matrix,Matrix> lidar::correctPoints(Matrix controlPoints, Pose3 poseInit, Pose3 poseFinal, Matrix relativePoints, Vector pointTimes) { 
    //init
    Matrix correctedPoints = gtsam::zeros(relativePoints.rows(),relativePoints.cols());
    Matrix posesEst = gtsam::zeros(6,pointTimes.size()); //might be duplicates?
    int nPoints = relativePoints.cols();
    double tEnd = pointTimes(nPoints-1);

    //controlpoint times
    int nControlPoints = controlPoints.cols();
    Vector tControlPoints = gtsam::zero(nControlPoints);
    

    //spline
    std::vector<double> tSp(nControlPoints),xSp(nControlPoints),ySp(nControlPoints),zSp(nControlPoints),
                        wxSp(nControlPoints),wySp(nControlPoints),wzSp(nControlPoints);
    for (int i = 0; i < nControlPoints; i++) {
        tControlPoints(i) = (double) i/((double) nControlPoints-1) * tEnd;
        //spline input vectors
        tSp[i] = tControlPoints(i);
        wxSp[i] = controlPoints(0,i);
        wySp[i] = controlPoints(1,i);
        wzSp[i] = controlPoints(2,i);
        xSp[i] = controlPoints(3,i);
        ySp[i] = controlPoints(4,i);
        zSp[i] = controlPoints(5,i);
    }
    tk::spline wxSpline,wySpline,wzSpline,xSpline,ySpline,zSpline;
    wxSpline.set_points(tSp,wxSp);
    wySpline.set_points(tSp,wySp);
    wzSpline.set_points(tSp,wzSp);
    xSpline.set_points(tSp,xSp);
    ySpline.set_points(tSp,ySp);
    zSpline.set_points(tSp,zSp);

    //compute corrected points with spline
    for (int i = 0; i < nPoints; i++) {
        posesEst(0,i) = wxSpline(pointTimes(i));
        posesEst(1,i) = wySpline(pointTimes(i));
        posesEst(2,i) = wzSpline(pointTimes(i));
        posesEst(3,i) = xSpline(pointTimes(i));
        posesEst(4,i) = ySpline(pointTimes(i));
        posesEst(5,i) = zSpline(pointTimes(i));

        //CORRECT POSE???
        Pose3 iPose = Pose3::Expmap(posesEst.col(i));
        // Pose3 iPose = lidar::logMapVecToPose3(posesEst.col(i));
        Point3 relativePoint = Point3(relativePoints.col(i));
        Point3 absolutePoint = iPose.transform_from(relativePoint);
        correctedPoints.col(i) = absolutePoint.vector();
    }

    return std::make_tuple(correctedPoints,posesEst);
}

Pose3 lidar::logMapVecToPose3(Vector logMapVec) {
    Pose3 tempPose = Pose3::Expmap(logMapVec);
    Matrix T = tempPose.matrix();
    T(0,3) = logMapVec(3);
    T(1,3) = logMapVec(4);
    T(2,3) = logMapVec(5);
    Pose3 pose = Pose3(T);
    return pose;
}


int main(int argc, char **argv) {
    ros::init(argc, argv, "lidar");
    lidar velodyne;

    ros::spin();

    return 0;
    
}

