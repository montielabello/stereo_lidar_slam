#include <ros/ros.h>  
#include <image_transport/image_transport.h>  
#include <cv_bridge/cv_bridge.h>  
#include <sensor_msgs/image_encodings.h>  
#include <sensor_msgs/PointCloud2.h>  
#include <opencv2/objdetect/objdetect.hpp>  
#include <opencv2/imgproc/imgproc.hpp>  
#include <opencv2/highgui/highgui.hpp>  
#include <opencv/cv.h>
#include <std_msgs/String.h>  

//region growing
#include <iostream>
#include <vector>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing.h>

using namespace std;

//point cloud callback
void pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
{
	ROS_INFO("inCallback");
	//get pointCloud in some workable format

	//call region growing function

	//fit get segments

	//keep planar segments
}

int main(int argc, char **argv)
{
  	ros::init(argc, argv, "lidar_planes");
  	ros::NodeHandle n;

	//test
	ROS_INFO("test");

	//pointCloud subscriber
	ros::Subscriber sub1 = n.subscribe("/kitti/velo/pointcloud", 1, pointCloudCallback);

  	ros::spin();

  	return 0;
}

