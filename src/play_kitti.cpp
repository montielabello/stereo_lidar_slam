#include "frontend.h"

/*#include <ros/ros.h>  
#include <image_transport/image_transport.h>  
#include <cv_bridge/cv_bridge.h>  
#include <sensor_msgs/image_encodings.h>  
#include <opencv2/objdetect/objdetect.hpp>  
#include <opencv2/imgproc/imgproc.hpp>  
#include <opencv2/highgui/highgui.hpp>  
#include <opencv/cv.h>
#include <std_msgs/String.h>  */

using namespace std;

//Left Image Callback
void imgLeftCallback(const sensor_msgs::ImageConstPtr& msg)
{
	//get image from msg, convert to opencv mat
	cv_bridge::CvImagePtr cvPtr;
  	try {  
    	cvPtr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);  
  	} catch (cv_bridge::Exception& e) {  
      	ROS_ERROR("cv_bridge exception: %s", e.what());  
      	return;  
  	}  

	//display
  	cv::imshow("imgLeftImage", cvPtr->image);
	cv::waitKey(10);
}

// Right Image Callback
void imgRightCallback(const sensor_msgs::ImageConstPtr& msg)
{
	//get image from msg, convert to opencv mat
	cv_bridge::CvImagePtr cvPtr;
  	try {  
    	cvPtr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);  
  	} catch (cv_bridge::Exception& e) {  
      	ROS_ERROR("cv_bridge exception: %s", e.what());  
      	return;  
  	}  

	//display
  	cv::imshow("imgRightImage", cvPtr->image);
	cv::waitKey(10);
}

int main(int argc, char **argv)
{
  	ros::init(argc, argv, "play_kitti");
  	ros::NodeHandle n;

	//windows for display
	cvNamedWindow("imgLeftImage");
	cvNamedWindow("imgRightImage");
	ROS_INFO("test");
	cvStartWindowThread();

	//subscribers
	ros::Subscriber sub1 = n.subscribe("/kitti/camera_gray_left/image_raw", 1, imgLeftCallback);
  	ros::Subscriber sub2 = n.subscribe("/kitti/camera_gray_right/image_raw", 1, imgRightCallback);

  	ros::spin();
	cvDestroyWindow("imgLeftImage");
	cvDestroyWindow("imgRightImage");

  	return 0;
}

