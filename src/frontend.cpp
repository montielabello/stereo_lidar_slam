#include "frontend.h"

using namespace std;

void frontend::disparity() {
	if (imgLeftFlag & imgRightFlag & infoLeftFlag & infoRightFlag) {
		ROS_INFO("disparity");
		// imgLeftFlag = 0;
		// imgRightFlag = 0;
		// infoLeftFlag = 0;
		// infoRightFlag = 0;
	}
}
// Left Image Callback
void frontend::imgLeftCallback(const sensor_msgs::ImageConstPtr& msg) {
	
	//testing ELAS
	if (!imgLeftFlag) {
		imgLeftMsg = msg;
		imgLeftFlag = 1;
		disparity();
	}

	// get image from msg, convert to opencv mat
	cv_bridge::CvImagePtr cvPtr;
  	try {  
    	cvPtr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);  
  	} 
	catch (cv_bridge::Exception& e) {  
      	ROS_ERROR("cv_bridge exception: %s", e.what());  
      	return;  
  	}  

	//display
  	cv::imshow("imgLeft", cvPtr->image);
	cv::waitKey(10);

}

// Right Image Callback
void frontend::imgRightCallback(const sensor_msgs::ImageConstPtr& msg) {
	
	//testing ELAS
	if (!imgRightFlag) {
		imgRightMsg = msg;
		imgRightFlag = 1;
		disparity();
	}

	//get image from msg, convert to opencv mat
	cv_bridge::CvImagePtr cvPtr;
  	try {  
    	cvPtr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);  
  	} 
	catch (cv_bridge::Exception& e) {  
      	ROS_ERROR("cv_bridge exception: %s", e.what());  
      	return;  
  	}  

	//display
  	cv::imshow("imgRight", cvPtr->image);
	cv::waitKey(10);
}

//left camera info callback 
void frontend::infoLeftCallback(const sensor_msgs::CameraInfoConstPtr& msg)
{
	
	//testing ELAS
	if (!infoLeftFlag) {
		infoLeftMsg = msg;
		infoLeftFlag = 1;
		disparity();
	}

	//front.imgLeftCount+=1;
	//if (!infoLeftFlag) {
	//	const sensor_msgs::CameraInfoConstPtr infoLeft = msg;
	//	infoLeftFlag = true;
	//}
}

//right camera info callback
void frontend::infoRightCallback(const sensor_msgs::CameraInfoConstPtr& msg) {

	//testing ELAS
	if (!infoRightFlag) {
		infoRightMsg = msg;
		infoRightFlag = 1;
		disparity();
	}

}

//point cloud callback
void frontend::pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr& msg) {
	//get pointcloud	
	sensor_msgs::PointCloud2 pointcloud = *msg;
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::fromROSMsg	(pointcloud,*cloud );

	//make these attributes/method of frontend class
  // pcl::search::Search<pcl::PointXYZ>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZ> > (new pcl::search::KdTree<pcl::PointXYZ>);
  pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
  // pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimator;
  // normal_estimator.setSearchMethod (tree);
  normal_estimator.setInputCloud (cloud);
  // normal_estimator.setKSearch (50);
  normal_estimator.compute (*normals);

  pcl::IndicesPtr indices (new std::vector <int>);
  // pcl::PassThrough<pcl::PointXYZ> pass;
  pass.setInputCloud (cloud);
  // pass.setFilterFieldName ("z");
  // pass.setFilterLimits (0.0, 1.0);
  pass.filter (*indices);

  // pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
  // reg.setMinClusterSize (50);
  // reg.setMaxClusterSize (1000000);
  // reg.setSearchMethod (tree);
  // reg.setNumberOfNeighbours (30);
  reg.setInputCloud (cloud);
  //reg.setIndices (indices);
  reg.setInputNormals (normals);
  // reg.setSmoothnessThreshold (3.0 / 180.0 * M_PI);
  // reg.setCurvatureThreshold (1.0);

  // std::vector <pcl::PointIndices> clusters;
  reg.extract (clusters);

  // std::cout << "Number of clusters is equal to " << clusters.size () << std::endl;
  // std::cout << "First cluster has " << clusters[0].indices.size () << " points." << endl;
  // std::cout << "These are the indices of the points of the initial" <<
  //   std::endl << "cloud that belong to the first cluster:" << std::endl;
  // int counter = 0;
  // while (counter < clusters[0].indices.size ())
  // {
  //   std::cout << clusters[0].indices[counter] << ", ";
  //   counter++;
  //   if (counter % 10 == 0)
  //     std::cout << std::endl;
  // }
  // std::cout << std::endl;

  pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = reg.getColoredCloud ();
  pcl::visualization::CloudViewer viewer ("Cluster viewer");
  viewer.showCloud(colored_cloud);
  while (!viewer.wasStopped ())
  {
  }

}

int main(int argc, char **argv) {
  	ros::init(argc, argv, "frontend");
  	frontend front;


	//windows for display
	cvNamedWindow("imgLeft");
	cvNamedWindow("imgRight");
	cvStartWindowThread();

  	ros::spin();
	cvDestroyWindow("imgLeft");
	cvDestroyWindow("imgRight");

  	return 0;
	
}

