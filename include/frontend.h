#ifndef FRONTEND
#define FRONTEND

#include <ros/ros.h>  
#include <ros/console.h>
#include <image_transport/image_transport.h>  
#include <cv_bridge/cv_bridge.h>  
#include <sensor_msgs/image_encodings.h>  
#include <opencv2/objdetect/objdetect.hpp>  
#include <opencv2/imgproc/imgproc.hpp>  
#include <opencv2/highgui/highgui.hpp>  
#include <opencv/cv.h>
#include <std_msgs/String.h>  

//region growing
#include <iostream>
#include <vector>
#include <pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing.h>

//elas
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <stereo_msgs/DisparityImage.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <image_transport/subscriber_filter.h>
#include <image_geometry/stereo_camera_model.h>
#include <pcl_ros/point_cloud.h>
//#include <elas_ros/ElasFrameData.h>
// #include <elas.h>

//frontend class
class frontend {
	//ROS subscribers
 	ros::Subscriber img_left_sub;
	ros::Subscriber img_right_sub;
	ros::Subscriber pointcloud_sub;
	ros::Subscriber info_left_sub;
	ros::Subscriber info_right_sub;

	//region growing stuff
	pcl::search::Search<pcl::PointXYZ>::Ptr tree;
	pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimator;
	// pcl::IndicesPtr indices (new std::vector <int>);
	pcl::PassThrough<pcl::PointXYZ> pass;
	pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
	std::vector <pcl::PointIndices> clusters;
	// pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud;

	//ELAS stuff
	bool imgLeftFlag;
	bool imgRightFlag;
	bool infoLeftFlag;
	bool infoRightFlag;
	sensor_msgs::ImageConstPtr imgLeftMsg;
	sensor_msgs::ImageConstPtr imgRightMsg;
	sensor_msgs::CameraInfoConstPtr infoLeftMsg;
	sensor_msgs::CameraInfoConstPtr infoRightMsg;

public:

	frontend() {
		//ROS subscribers
		ros::NodeHandle nh("~");
		int buffer = 20;
		img_left_sub   = nh.subscribe("/kitti/camera_gray_left/image_raw",buffer,&frontend::imgLeftCallback,this);
		img_right_sub  = nh.subscribe("/kitti/camera_gray_right/image_raw",buffer,&frontend::imgRightCallback,this);
		pointcloud_sub = nh.subscribe("/kitti/velo/pointcloud",buffer,&frontend::pointCloudCallback,this);	
		info_left_sub  = nh.subscribe("/kitti/camera_gray_left/camera_info", buffer, &frontend::infoLeftCallback,this);
		info_right_sub = nh.subscribe("/kitti/camera_gray_right/camera_info", buffer, &frontend::infoRightCallback,this);
		// pointcloud_sub = nh.subscribe("/velodyne_points",buffer,&frontend::pointCloudCallback,this);	

		//region growing stuff
		tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZ> > (new pcl::search::KdTree<pcl::PointXYZ>);
		normal_estimator.setSearchMethod (tree);
		normal_estimator.setKSearch (50);
		pass.setFilterFieldName ("z");
		pass.setFilterLimits (0.0, 1.0);
		reg.setMinClusterSize (50);
		reg.setMaxClusterSize (1000000);
		reg.setSearchMethod (tree);
		reg.setNumberOfNeighbours (30);
		reg.setSmoothnessThreshold (3.0 / 180.0 * M_PI);
		reg.setCurvatureThreshold (1.0);

		//ELAS stuff
		imgLeftFlag = 0;
		imgRightFlag = 0;
		infoLeftFlag = 0;
		infoRightFlag = 0;
	}
	
	~frontend() {}

	void imgLeftCallback(const sensor_msgs::ImageConstPtr& msg);
	void imgRightCallback(const sensor_msgs::ImageConstPtr& msg);
	void pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr& msg);
	void infoLeftCallback(const sensor_msgs::CameraInfoConstPtr& msg);	
	void infoRightCallback(const sensor_msgs::CameraInfoConstPtr& msg);
	void disparity();

};

#endif
