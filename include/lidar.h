#ifndef LIDAR
#define LIDAR

#include <ros/ros.h>  
#include <ros/console.h>
#include <image_transport/image_transport.h>  
#include <cv_bridge/cv_bridge.h>  
#include <sensor_msgs/image_encodings.h>  
#include <opencv2/objdetect/objdetect.hpp>  
#include <opencv2/imgproc/imgproc.hpp>  
#include <opencv2/highgui/highgui.hpp>  
#include <opencv/cv.h>
#include <std_msgs/String.h>  
#include <tuple>
#include <math.h>

//region growing
#include <iostream>
#include <vector>
#include <pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing.h>

//RANSAC
#include <pcl/ModelCoefficients.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

//GTSAM
#include <gtsam/base/Vector.h>
#include <gtsam/base/Matrix.h>
#include <gtsam/geometry/Point3.h>
#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Pose3.h>

//Spline
#include <cstdio>
#include <cstdlib>
#include "spline.h"

//velodyne_spherical_laserscan_node
// #include <velodyne_spherical_laserscan/packet_data.h>
#include <spherical_scan_msgs/Scan.h>
// #include <dynamic_reconfigure/server.h>
// #include <velodyne_pointcloud/CloudNodeConfig.h>

//ICP
#include <pcl/point_types.h>
#include <pcl/registration/icp.h>

//TF
#include <tf/transform_broadcaster.h>

using namespace std;
using namespace gtsam;

//------------------------------------------------------------------------------------------
//lidar class
class lidar {
	//ROS subscribers
	ros::Subscriber pointcloud_sub;
	ros::Subscriber spherical_scan_sub;
	ros::Publisher  pointcloud_pub;
	ros::Publisher  corrected_pointcloud_pub;

	//store points as matrix
	Matrix cloudMat;

	//plane indexes
	Vector iPlanes;
	Matrix planeParameters;

	//flag - testing
	int scanCount = 0 ;

	//region
	int nRegions;
	int nPlanes;

	//from spherical_scan
	Vector timestamps;
	Matrix points;
	Matrix dirs;

	//ICP prior
	pcl::PointCloud<pcl::PointXYZ>::Ptr prevCloudPtr;
	Pose3 prevPose;
	sensor_msgs::PointCloud2 prevPointcloud;

public:

	lidar() {
		//ROS subscribers
		ros::NodeHandle nh("~");
		int buffer = 1;
		// pointcloud_sub = nh.subscribe("/velodyne_points",buffer,&lidar::pointCloudCallback,this);	//pointcloud from rosbag
		pointcloud_sub = nh.subscribe("/velodyne_spherical_points",buffer,&lidar::pointCloudCallback,this); //pointcloud from spherical_scan
		// pointcloud_sub = nh.subscribe("/kitti/velo/pointcloud",buffer,&lidar::pointCloudCallback,this);	
		spherical_scan_sub = nh.subscribe("/velodyne_spherical_scan", buffer, &lidar::scanCallback,this);
		pointcloud_pub = nh.advertise<sensor_msgs::PointCloud2>("/velodyne_spherical_points", 1, true);
		corrected_pointcloud_pub = nh.advertise<sensor_msgs::PointCloud2>("/corrected_points", 1, true);

		const Vector idVec = gtsam::zero(6);
    	prevPose = Pose3::Expmap(idVec);

	}
	
	~lidar() {}


	void pointCloudCallback(const sensor_msgs::PointCloud2ConstPtr& msg);
	std::vector <pcl::PointIndices> planarRegions(const sensor_msgs::PointCloud2ConstPtr& msg);
	std::vector <pcl::PointIndices> regionSegmentation(const sensor_msgs::PointCloud2ConstPtr& msg);
	void scanCallback(const spherical_scan_msgs::Scan& msg);
	Pose3 scanOdom(pcl::PointCloud<pcl::PointXYZ>::Ptr prevCloudPtr, pcl::PointCloud<pcl::PointXYZ>::Ptr currentCloudPtr);
	std::tuple<Matrix,Matrix,Pose3> unwarpLidar(Matrix relativePoints,Pose3 poseInit,Pose3 poseFinal, Vector pointTimes, std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes);
	Matrix solveLM(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes, std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes);
	Matrix solveGN(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes, std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes);
	Matrix solveDL(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes, std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes);
	Matrix computeJacobian(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes, std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes);
	Vector computeResiduals(Matrix controlPoints, Pose3 poseInit,Pose3 poseFinal, Matrix relativePoints, Vector pointTimes, std::vector <pcl::PointIndices> planeIndexes, Vector iPlanes);
	std::tuple<Matrix,Vector> fitPlanesFromCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::vector <pcl::PointIndices> clusters, int iRegion);
	std::tuple<Matrix,Vector> fitPlane(Matrix planePoints);
	std::tuple<Vector,Matrix> initControlPoints(int nControlPoints, double tStart, double tEnd, Pose3 poseInit, Pose3 poseFinal);
	std::tuple<Matrix,Matrix> correctPoints(Matrix controlPoints, Pose3 poseInit, Pose3 poseFinal, Matrix relativePoints, Vector pointTimes);
	Pose3 logMapVecToPose3(Vector logMapVec);
};

#endif
